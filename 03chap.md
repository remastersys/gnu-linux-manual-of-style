## 3.0 Paths and Permissions
The common issues in documentation or when working with GNU Linux falls into 3 categories:
• Paths
• Permissions
• Dependencies
## Permissions
### Group, user
In documentation, the user may need to run commands or execute files as root. The documentation must be clear and easily follow which permissions are needed.
### Use chmod
In order to run scripts, users need to chmod the script setup.sh first.
Veteran GNU Linux users already know this. However, this may not be obvious to new users. Create a reusable blurb guiding users to chmod. This content can be reused each time.

Before running the script make the script executable by running:

chmod a+x nameofscript.sh
## 3.1 Dependencies
### In Scripts
When working with developers or scripts, indicate the manner for installing dependencies in the style guide – code section. Some projects call a list of dependencies rather than explicitly declare the dependencies within the script. This way, when dependencies change, the script remains the same but the dependencies file called by the script will change.
## 3.2 Links
Guidance to other information does require some maintenance and support.
### Include information about the URL
Always include information about the URL as the link to a 3rd party may change.

Correct: Display the title of the content and link
For more information about Free Culture works, see the blog post, FOSS Content Creator Series: Is your content considered "Free Cultural Works"?, at:
https://software.intel.com/content/www/us/en/develop/blogs/approved-for-free-culture-works.html

Correct: Display the title of the content linking
For more information about Free Culture works, visit FOSS Content Creator Series: Is your content considered "Free Cultural Works" in the developer zone.

Incorrect: No information about the link with period directly after the URL
For more information about Free Culture works, visit here.
### Avoid ending a sentence or placing a period directly after the URL
In the incorrect example, the link will work as the period is not included in the URL. Careless formatting during the publication process may make the URL or link unreachable.

Correct: Content after link
For more information about Free Culture works, visit FOSS Content Creator Series: Is your content considered "Free Cultural Works" in the developer zone.

Incorrect: Period directly after URL
For more information about Free Culture works, visit FOSS Content Creator Series: Is your content considered "Free Cultural Works".
### Use correct language. Avoid “on” when “about” can be used
Correct: Use about
For more information about Free Culture works, visit FOSS Content Creator Series: Is your content considered "Free Cultural Works" in the developer zone.

Incorrect: Using on
For more information on Free Culture works, visit FOSS Content Creator Series: Is your content considered "Free Cultural Works" in the developer zone.
## 3.3 Code
Formatting of the code itself is usually developer choice. Some C developers format using K&R. Some use other formatting. Python programmers may use the python style guide.
Spaces or tabs? Kernel developers may use the kernel guide.
This section offers guidance for placing code in documentation, not formatting code.

Often tedious commands are inserted in documentation one command at a time. This could be improved by providing a script. Windows writers or developers may not be as familiar with running commands or working in the shell. Limited experience with bash will slow down the process, requiring the developer to do more work. This could end up costing the project or company more money. Where efficiency is imperative, use an experienced GNU Linux Technical Communicator/Content Creator.

When presenting commands like this, remember to format the commands properly with functional dashes. Remember to include only the command in the copy code snippet (no results). Do not include command prompts in copy code snippets.

Remember, offer information about the commands where this makes sense. In other words, offer some information.

Incorrect:	Run make to make the application.
Correct:	Run make to build the application.
### General guidelines
Code snippets or examples are a necessary component for tutorials, application notes, and demos.  Often code is presented in Courier font.

Presenting functional code snippets with copy code functionality is an important task to undertake. The way code snippets are presented will be used for some of the following:

• assist reader work through a demo
• assist reader understand the code involved
• establish credibility
• establish reliability
• build relationships with other developers

The content creator may not be familiar with GNU Linux development or the community. However, what the author presents in code and how code is included is an essential part of building a community and moving your project forward.

Other considerations when writing for developers includes whether to use proprietary products, or respect freedoms and privacy. Community culture is important when writing for GNU Linux.
### Guidance - Per GPL 1.3:
“If your document contains nontrivial examples of program code, we recommend releasing these examples in parallel under your choice of free software license, such as the GNU General Public License, to permit their use in free software.”
### Commands
Many applications, tools and utilities are run as commands at the shell. There are many available GUI tools. However, using the shell is a lot faster and you can monitor progress in the shell.
### Text with Code
Insertion of text with code or insertion of code within text is distracting and can be frustrating.

Correct:
You are ready to install! Run the command to install midnight commander.

sudo apt install mc

Incorrect:
You’re ready to install! Type

sudo apt install mc

and press Enter. This will install midnight commander.

The incorrect example above is not correct for the following reasons:
• uses contraction
• breaks up instructions with command
• instructs users to “type” and “enter” instead of actual action of running a command
### Applications, Tools, Utilities
These applications, tools and utilities are not called, titled or referenceed as “ <appname> Command Line application”.
#### Example:
If we run ffmpeg at the command line, this is not considered  a cli app. In the text we do not add “command line” to the name of the application. When working with novice GNU Linux developers, the technical writer must relay information important to documentation and be aware of jargon and culture.

Incorrect: 	ffmpeg command line application
Correct:	ffmpeg

In fact, there may be an application created for GNU Linux that opens a shell and runs. However, this is not typical. That type of application may be called or title “unique command line application”.
### Flags, Options or Args
Listing flags available or common to the application may be useful for the reader. Not everyone understands how to use a man page. Your project may have a man page, a reference doc or possibly use a help option to list commands. Either way, listing flags in documentation is best done by using a table.
#### Example: Commands one at a time or as a script
Often, instructions will include many commands throughout a document. While the instructions are meant to lead the reader to tediously run one command at a time, a script included with the documentation, especially for web content is recommended.
#### Example:  One command at a time.
First update and upgrade.

apt install -y update && apt install -y upgrade

Use these explicitly declared packages in this command to install depends.

apt install all these packages -y

See best practices for more information about using a file including all the depends with a script and why, explicitly declaring these one by one is not efficient.

### Code snippets
When creating documentation with code snippets, execution of the commands is vital. Include a file to download all commands or a script where possible. Make commands functional when pasted.

• command prompts in code snippets
• wrong non Posix dash in code snippets
• results in code snippets
• command prompt in copy code as root while requiring sudo
• wrong quotation mark
• too many comments in copy code snippets
#### Example: Command prompts in copy code snippets
When command prompts are copied in copy code snippets, the command will fail. This is a waste of the developer time, often considered an inconvenience or inconsiderate. When an individual does this, the community or developer may be more “forgiving” than when a company does this.

Correct: 	apt install mc
Incorrect: 	# apt install mc
$ apt install mc
#### Example: Wrong or not POSIX, not functional dash in code snippets
When the content creator uses Windows or LibreOffice, the not functional dash can easily be inserted into GNU Linux commands. This dash will not work in the shell or at the command line.

Correct: 	apt install mc -y
Incorrect: 	apt install mc –y
#### Example: Command prompt in copy code as root while requiring sudo
Consider the situation - would you use sudo as root? In what case? In this case?

Correct:	sudo apt install mc
Incorrect:	# sudo apt install mc
### Results
Do not include results in copy code snippets.
When displaying results with the command, if this is in a copy situation, this is not recommended. Depending on the project, use a table or separate commands from results. Clearly present results from a command.

command
result
lscpu
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Thread(s) per core:    1
Core(s) per socket:    4
.
.
.

The following are results when running the dpkg command. If the system has midnight commander installed, this will display in results as installed.

Correct: Separate command from results
Command to check if midnight commander is installed.

dpkg --get-selections | grep mc

Results
libmcrypt4					install
mc						install
mc-data					install

Incorrect: including results in copy code snippet
Command to check if midnight commander is installed.

dpkg --get-selections | grep mc
libmcrypt4					install
mc						install
mc-data					install
#### Example: Wrong quotation mark
When the content creator uses a text editor, often quotation marks will convert. One method for applying quotation marks in code to a text editor is:

Copy the code from an IDE and paste the code directly into a text editor without making edits to the code.

Incorrect: 	`throw` or ‘throw’
Correct: 	'throw'
### Bulleted code snippets
There are rare situations where code snippets may be a bulleted list.
Include in the “lists” section for unenumerated lists, the guidance for bulleted lists.
For the purposes of creating content for R00T or FAQ Linux, do not place code in lists unless there is a really good reason.
### Error in result
Errors in results need troubleshooting information. Do not include an error in a result within a copy code snippet.
#### Example: Including errors in copy code snippets.

Incorrect:  	Error included within copy code snippet

apt install mc
E: Could not open lock file /var/lib/dpkg/lock-frontend - open (13: Permission denied)
E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), are you root?

Correct: 	No error in copy code snippets

apt install mc
### No gutter numbers for commands
Distracting gutter number for commands do not belong in copy code snippets or in examples in GNU Linux content.
In some cases, such as Drupal Syntax Highlighter, the gutter number is not copied with the code in the snippet when the user copies the command. However, the number to the left of a single command is distracting and not recommended.

In fact, do not place gutter numbers to the left of single commands run or executed at the terminal, command line, or shell.

Incorrect:	Gutter numbers to the left of commands in copy code snippets

1	apt install mc

Correct:

apt install mc
## Configuration files
Guidance to edit config files are common. This guide recommends creating a backup of the original config file and running commands to edit config files.
### Inform users to make an orig or backup of the config file
To make a simple backup of the original config file, instruct users to run a copy command. Create a reusable blurb for your project such as the blurb found below.

Create a backup of the original file prior to editing the config file by running:

sudo cp filetoedit.config filetoedit.config.orig
### Inform user to edit file
The information for editing a file may differ. Running a command using bash is highly recommended for developer experience.  An example is replacing content in a config file or appending content to a config file. To append at the end of a file, offer a command to the user instead of instructing users to manually edit the config file, where possible.

echo "this line" >> filename.cfg

### Example:  Command Prompts, Comments, Variables
Usage/Description

```
#!/bin/bash  
Shebang

# Author:  marcia 'aicra' wilbur
# Usage:   grep character path/to/*.md
# Example: grep – filename.md
Comment

result=$(grep – filename.md)
echo $result
Variable

aicra@faqlinux:~$
Command Prompt - user

root@faqlinux:~#
Command Prompt - root
```
## 3.4 Audience and Tone
Tone is very important in documentation. Never write an article or tutorial like an advertisement.

Technical marketing may have good input and manage company branding with a marketing communications workflow. However, writing articles like an ad, shilling articles, paying for articles or content without transparency is never recommended. Be transparent. Create good content. Keep ethics in engineering principles in writing as well.

• Keep content informal, not casual.
• Clear
• Concise

In summer 2017, Arizona State University held a Marketing Communications day or communications camp at the Polytechnic campus. During this event, the keynote speaker stated, data showed our “students like it when we seem sincere.” This part of the presentation was centered around the need to “seem sincere”. What about, “being sincere”! When you write, do not write for an ad or to market for the sake of marketing.

• Be sincere.
• Be transparent.

Define a primary and secondary audience to set the voice and tone. See chapter 1 for the template for audience analysis. Define the tone based on the audience.
## Tone Guidance – Google Dev Style
While we do not entirely agree with Google Style, the guidance for tone is sound advice. For some reason “aim” is used a few of times and while we do not recommend using “aim” or starting sentences with “But” - the overall tone guidance is nice. At FAQ Linux, we want the authors to “be human”!

The following is tone guidance from the Google Developer Style Guide and is licensed under Creative Commons Attribution 4.0 License and the code is Apache 2.0 License.
Aim, in your documents, for a voice and tone that's conversational, friendly, and respectful without being overly colloquial or frivolous; a voice that's casual and natural and approachable, not pedantic or pushy. Try to sound like a knowledgeable friend who understands what the developer wants to do.
Don't try to write exactly the way you speak; you probably speak more colloquially and verbosely than you should write, at least for developer documentation. But aim for a conversational tone rather than a formal one.
Don't try to be super-entertaining, but also don't aim for  super-dry. Be human, let your personality show, be memorable; you can even be a little funny now and then. But remember that the primary purpose of the document is to provide information to someone who's looking for it.
Remember that many readers are not native English speakers, many of them come from cultures different from yours, and your document may be translated into other languages.

Aside from using guidance for tone from Google, you can base your style guide on other style guides and build from there – in the same way you would build a custom distro!
There were items listed in the Google style we do not agree with. In fact, we didn’t even agree with the title use of “where”. Those items are listed in the table, Google: Some things to avoid where possible.
Google Guidance
FAQ Linux Guidance
Buzzwords or Technical Jargon
Disagree - culture sensitivity and “street cred” usage
Being too cutesy
Agree, but do you.
Ableist language or figures or speech
Depends.
Placeholder phrases like “please note” and “at this time”
Disagree - upcoming information and notes are important in development. Be honest overall. No smoke and mirrors or vapor.
Choppy or long-winded sentences.
Agree - remember the tech writing Cs
Clear, Concise, Credible
Starting all sentences with the same phrase (such as You can or To do).
Agree - but practice what you preach, Google!
Current pop-culture references
Disagree - content maintained correctly can use “current“ cultural terms and references. In free and open source, community and culture this is huge.
Jokes at the expense of customers, competitors, or anyone else.
Moderately Disagree - as long as it is in good taste and appreciated by a friendly competitor.
Example: I don’t do Windows!
We also welcome the use of M$.
Exclamation marks, except in rare really exciting moments.
Disagree!!!!!
Wackiness, zaniness, and goofiness.
Disagree - depends on content
Mixing metaphors or taking a metaphor too far.
Disagree – being honest is good, clear metaphors is how some of us communicate!
Funny lines that aren't closely related to the topic, or that require a lot of off-topic verbiage, or that obscure information.
Funny lines?
Like ~~~~~~~~?
Disagree – fun is good!
Internet slang, or other internet abbreviations such as tl;dr or ymmv.
Disagree. Use acronyms and abbreviations. If there is a concern the audience does not know the term, add a terminology table in the introduction or overview section.
Google Guidance
FAQ Linux Guidance
Phrasing that denigrates or insults any group of people.
Agree - to a point. “Any” goes a bit far.  Too general. One would hope as a writer denigration would be left out. Makes you wonder:
WHY this is in Google’s style guide! WHAT happened?
Insulting proprietary heathens, code of coduct enforcers, cancellors and fashionable open source or fauxpen source is encouraged for R00T magazine journalistic articles. Insults and content denigrating groups of people or users in user guides or training is not recommended!
Let's do something positive in the culture war.
Using phrases like simply, It's that simple, It's easy, or quickly in a procedure.
Agree. Avoid “simply” and “easy” or “easily”. Also avoid “roughly”, “complex” where you can. Disagree. Use “quickly” when the process is quick. Simple is relative. Time is measurable based on ability also, but can be generalized in approximate measures.
## 3.5 Grammar
### Any, All, Everything, Nothing
Do not use generalizations.
### Contractions
Avoid contractions.

In the Google Style, notice the use of contractions. Contractions are not recommended for technical documentation. While some style guides recommend contractions, this is not recommended in this style guide. We do not recommend.

Correct: 	Do not use contractions!
Incorrect: 	Don’t follow Google’s direction



## 3.6	 Punctuation
Aside from specifying punctuation rules of thumb for a project, watch for “random” or “stray” punctuation, especially in code snippets. Use a content creator who can catch any random punctuation. In one recent case, a random hyphen was found after .sh in a command. Another issue is “stray periods” after commands or code.

Incorrect:	sudo ./runthis.sh -
Correct: 	sudo ./runthis.sh

Incorrect:
To view the built-in dev-server, point the browser to http://localhost:8000.

Correct: 	
Open a browser. To view the built-in dev-server, visit:

http://localhost:8000
## Commas
Commas are project specific. A project may use the serial or Oxford comma. Other projects may choose not to use the Oxford comma.
### Serial or Oxford Comma
The serial comma uses commas for this, that, and the other.
### General Comma Usage
Use a comma before “and”, “or” and “but” when the following is a sentence.

Correct: Insert the microSD card into the slot, and start the Raspberry Pi.
### Single Commas
Use single commas with city and state.
Correct: Visit Mystic, Connecticut this summer for delicious clam bellies.
##  Period
Avoid using periods at the end of commands or directory paths.
Decide whether to use periods at the end of bulleted or enumerated lists.
##  Hyphens and Dashes
hyphen 					
The POSIX hyphen is used in commands   	
-
apt  install mc -y
en dash					
used with time periods or range		
used as a minus sign 				
–
15–20 minutes
–12
em dash					
used to emphasize a section of a sentence that may otherwise be in parens or parenthesis.
—
There are several options — hyphen, en dash, em dash — available when writing documentation.

##  Exclamation Point
Use these where needed. Our content and publications audience appreciate a casual tone. Other guides advise against exclamation points.

Seriously!
##  Pipe
### What is it?

Pipe 	|

Used  in content as a verb or noun.
### How it works
The pipe character is used in commands to send data from a program.
### Example
Pipe into grep to see which OpenCV tools are installed.

dpkg --get-selections | grep opencv
## Colon
When using a colon with a note, do not bold the colon.
Note:    This is an example.

## Quotation Marks
Use correct quote marks within code.
This is very important when working with bash and python.
There could be conversions from straight quotes to other non functional quote marks. Watch for this.
A thorough check could be part of the editing workflow.
## Apostrophes
Avoid using apostrophes in a way to personify inanimate objects.

Correct:      The code was found on Joe’s computer.
Incorrect:   The computer’s hard drive included the code.
## Chevrons
Use correct chevrons and do not edit in chevrons in code unless the code chevrons are not correct.

Correct:    dpkg --get-selections >> installed
## Parens
Select how parens are used within the project. The first use of acronyms often use parens with the full
phrase with the use of the acronym after.
## Slash
Use the correct slash in paths and commands. For other operating systems, the slash used with paths and commands are different from paths in GNU Linux.

Correct:
  cd /etc/apt

Incorrect:
  cd \etc\apt

Slashes are also used in bash and the correct slash must be indicated for scripts and commands.
## 3.7	User (Normal User, Root User, End User)
### User
User is a good description for users.
### Normal user
Never use “normal user”.
### Developer
Developer is a good description for developer users.
### End User
Personal preference. Some projects use the term “end user”, while some do not.
###  Root User
Do not use “root user”. Instead instruct the user or reader to become root or run as root.
## 3.8 Naming (example: Pop OS! Greek translation)
When naming your project or product, remember to research the name. Some past examples are “Nova” and “Pop OS!”
In Greek, Pop OS! Translates to “ass”.

Note: Distros like Pop OS! are “trademarked”. Therefore, if you like the look and feel of the OS, change the plymouth theme, add your branding and create your custom iso or image. Anyone can customize a distro – especially Debian based distros.
## 3.9 Communications and Contents
### Please
Do not use the word “please” in documentation. Using imperative or direct language is important.

• Connect the servo.
• Program the Arduino Nano.
• Upload the code to the chip.

### Working with Engineers
In a factory setting, there was a cart with a printed page on the flat top.

Example: Please do not place anything on this cart!

This was not readable because there were too many items, including acetone bottles, on the cart.
When the technical writer informed the mechanical engineer about this issue, the technical writer recommended removing the word “please” from the notice.

The mechanical engineer replaced the cart with a slanted cart, eliminating the need for any notice.

Technical writers can communicate root cause, observations and work with engineers and developers to build solutions.

# Solutions = right people, right skills, right tools
