# Chapter 2. GNU Linux Style Guidelines

The following guidance Includes FAQLinux GNU Linux copy style guidance for content and contributions to FAQ Linux website and magazine, R00T.

These guidelines are in no way considered a standard outside FAQLinux. However, many considerations in this chapter may be useful for your GNU Linux content.

This guide is available and free to use in a non-commercial way.
If you have any questions, please contact the author, Marcia K Wilbur at

aicra@faqlinux.com

FAQ Linux will never ask you to collaborate with proprietary tools or platforms/services such as Google Docs, Notion, Dropbox or anything similar.

We do have a GitLab available at:

https://gitlab.com/remastersys/FaqLinux
Preferred content file types – FAQ Linux
md - markdown
html
xml

## Example: Font Style
FreeSans or san serif font
Aa
abcdefghijklmnopqrstuvwxyz
ABCDEFGHIJKLMNOPQRSTUVWXYZ
1234567890
##  Headings
Headings are an important part of content strategy. With headings, the  reader can locate information efficiently and quickly. With technical documentation, this is essential Users do not always read the entire guide, but will skip sections.  An example may be an installation section in a get started guide. Installation may be as simple as apt install <tool>. Therefore, there really is not a lot of need for installation guidance, depending on your project. Keep installation sections simple.
# Heading 1
The following Heading 1 style is found below.
Font Type:	FreeSans
Font Style:	bold
Font Size:	18 pt
Spacing above and below are set to .06 in this example.
##  Heading 2
The following Heading 2 style is found below.
Font Type:	FreeSans
Font Style:	bold
Font Size:	16 pt
Spacing above and below are set to .06 in this example.
###  Heading 3
The following Heading 3 style is found below.
Font Type:	FreeSans
Font Style:	bold
Font Size:	14 pt
Spacing above and below are set to .06 in this example.
####  Heading 4
The following Heading 4 style is found below.
Font Type:	FreeSans
Font Style:	bold
Font Size:	16 pt
Font Color: 	729fcf
Spacing above and below are set to .04 in this example.

## Images
The images included with each heading style indicate the style of each heading. The image settings are found below. Using a gray rather than black is recommended.
###  Image Captions
Use Figure, if possible and generate a list of figures. Original images are best. When using external images, use proper references, get permission and/or use correct licensing and attribution.

Use a standard size for images. We recommend 5.5 for instructions.
Your project may require a different size. Keep image size similar or consistent.
If size cannot be consistent, a bounding box of consistent size can be placed around images .
























Figure 1. Example Image 5.5 with border























Figure 2. Bounding Box Method
### Image Settings

Width: 	5.5
Border color: 	Light Gray 2 (in LibreOffice)
Border Line weight: 	.05 pt
Border Alignment:	Left

#### Caption Settings
The style for captions is found below.
Font Type: 	FreeSans
Font Style: 	Emphasis or Italics on the word Figure X.
Font Style: 	No emphasis for the description.
Font Size: 	10 pt
Font Alignment: 	Left

Figure X. This description
Callouts
Use the colors and brand for your project.
Use good contrast – avoid red except in case of emergencies!
#### Magnification
Magnification is a great way to  identify content within an image as a callout. Since many projects may not have technical graphic assistance, an application or tool to create this type of callout would be extremely useful.

Alternately, use a template and GIMP or an image editor to create magnified callouts. This is different from zooming in on images in Drupal or on the web.
#### Icons
Adding icons offers visual cues to readers. These cues offer great navigation guidance. Remember to use clear and useful icons.

## 2.2 Copy Code
Often in documentation, copy code snippets can be useful for the end user. One thing to consider is how many copy code snippets are presented in a tutorial or demo instructions.

What may be more useful is a script or attached script containing the tedious commands.
See Best Practices for more information.
For the purposes of this section, decide how copy code snippets are formatted.

Font:	Courier or Mono
Size:	11
Weight: 	None
Font Color:	White
Background Color: 	Black
## General Content
The following guidance  sets the tone for the documentation or content. There are some considerations here you can apply to your own style guide or brand also. For example, some documentation includes a boundary or bounding box around the images. This can be used to center the image without distraction of varied sizes.
## Files
### Naming convention
Keep file names short.
Never use spaces.
Use CamelCase, _ underscore, or – dashes instead.

Correct: thisfile.md
Incorrect: this file.md

##  Character width – text and terminal
Some studies indicate 50 characters a line makes content more readable. Then, there are other studies stating 50 characters a line is too short, and 100 characters is the ideal line width. Then, of course, the default length of a terminal is 80 characters.

Decide how wide the terminal will display. If a screen capture is needed and the width must be adjusted, do so when the documentation is improved by these adjustments. 80 characters may not be wide enough to clearly display the command.

In addition to the guidance presented here, remember to use styles.
Active Voice
Use the present tense in third person (user) without pronouns with an active voice. Use strong verbs to indicate action.
## Personification
Do not use personification in technical documentation.  Inanimate objects are never “they” or “them”.

Correct:	List the tools in hardware section.
Correct:	List these in hardware section.
Incorrect:	List them in the hardware section.
Localization
Use simple English with simple words for quality translation.
Example: Choose use over utilize.

• Avoid cliches, idioms, and jargon in documentation for multiple languages.
• Avoid nouns used as verbs.

Example action verbs are listed.

act
add
analyze
apply
arrange
assign
assist
author
automate
build
change
collaborate
communicate
define
deliver
demonstrate
describe
develop
edit
function
generate
update
upgrade
use
validate
verify

### Be consistent with terms . Decide which term or word to use throughout documentation.
Example: “terminal” “shell” “xterm”

### Be consistent with capitalization.
Example: localhost is preferred over Localhost or LocalHost
Often, projects have several different guidance regarding capitalization of a brand. To stay in compliance with trademark, set guidance to writers for consistence.
### Simplify Complicated Topics
One of the main duties of a technical writer can be to simplify content for readers.
Create easy to read content
Write short sentences
Avoid ambiguity
Styles
Applying styles is one way to save time when creating documentation. In the early 2000s, this started to be a standard or expectation for technical writers.

Of course, using styles is not indicative of talent or skill at all! LibreOffice does use styles for their documentation. Why doe this make sense? If the branding changes, editing a style takes only a minute or so. Manually editing or formatting all the text would take additional time - A lot of additional time!
The following information about character/paragraph styles is from the LibreOffice Writer Guide 6.0 and is licensed under Creative Commons Attribution 4.0 License or later and GNU General Public License (http://www.gnu.org/licenses/gpl.html), version 3 or later.

# What are styles?
Most people are used to writing documents according to physical attributes. For example, users
may specify the font family, font size, and weight (for example: Helvetica 12pt, bold). In contrast,
styles are logical attributes. For example, define a set of font characteristics and name this,
“Title” or “Heading 1”. In other words, styles shift the emphasis from what the text
looks like to what the text is.
# Why use styles?
Styles help improve consistency in a document. Styles also ease formatting modifications.

For example, the indentation of all paragraphs or font of all
titles must be updated. For a longer document, this simple task could be prohibitive. Styles ease this task and saves time.
Style categories
LibreOffice Writer has six style categories:
• Paragraph styles affect entire paragraphs and are also used for purposes such as
compiling a table of contents.
• Character styles affect a block of text inside a paragraph; they provide exceptions to
paragraph styles.
• Page styles affect page formatting (page size, margin, and the like).
• Frame styles affect frames and graphics.
• List styles affect outlines, numbered lists, and bulleted lists.
• Table styles affect the appearance of tables of data.

Caution:  Manual formatting (also called direct formatting) overrides styles. You cannot get rid of manual formatting by applying a style to it. To remove manual formatting, select the text and choose Format > Clear Direct Formatting from the Menu bar, or select the text and press Ctrl+M.
## Working with character styles
Character styles provide exceptions to the formatting in a paragraph style. They are applied to
groups of characters, rather than whole paragraphs. They are mainly used when you want to
change the appearance or attributes of parts of a paragraph without affecting other parts.
Examples of effects that can be obtained by means of character styles are bold or italic typeface
or font colored text words.


## Notes, Warnings, Important, Caution
Notes are a necessary component in technical documentation. Remember to provide notes, warnings, cautions and important information in the correct location. For example, a warning belongs prior to an action. If the warning comes after a directive to action, the user is not properly informed prior to execution of the action and the warning is not as useful.

San serif font		12 	Bold 	

Note:	Bold the word Note. Do not bold the colon. Tab, not space before text. Wrap. Some projects may want a color background.

In some cases, your colors will reflect your brand.

Note: Some projects may want a color background.
Color
Undefined – per your branding
Page
Letter 8.5 x 11
Margin
Top: 0.5
Bottom: 0.5
Left: 0.5
Right: 0.5
Header
Decide whether to include a header, what information to include in the header, alternating content, a header with logo with section variables to indicate which section/chapter based or no header at all.
Footer
Select footer content based on project preferences.
The footer can consist of a document number, section or page numbers.  Also, the footer can have alternating odd and even content in the footer.

Document Name, Number or Revision (rev xx) 	Lower Left corner
Page Number	Page 1 of x Lower Right corner

Some projects may want to alternate between left and right locations for the page numbering.
### Tables
Remember to use table numbers, if possible and even generate a list of tables based on styles and table generation.
#  General Table Guidance
Table Rows:	Depends on contents
Table Columns:	Depends on content
Table Color:	Alternate gray and white every other row or another set of colors.

Table Text Font
Font Type:	FreeSans
Font Size:	10 pt
Font Color:	Black
Row Color:	Alternating Light Gray 4 background with White
Font Weight:	None

Table Heading Row
Font Type:	FreeSans
Font Size:	10 pt
Font Color:	Black
Row Color:	Light Gray 2
Font Weight:	Bold

Table Heading Row
Value
Font Type
FreeSans
Font Size
10 pt
Font Color
Black
Row Color
Gray
Font Weight
Bold

Table 1. Table Values

Larger fonts can be used. Modify the table format to your project requirements.
Often, lists can be converted to tables.
## 2.4 Capitalization
### Sentence case
Sentence is used for most headings. Heading 1 can be uppercase.
Table headings and captions are sentence case.
### Case
The options are:
• Sentence case
• Upper Lower Case
• UPPERCASE
• lowercase
There are several choices. Remember to select the type of case for headings and text for use in content, to keep the documentation consistent.
## 2.5 Spelling
FAQ Linux and R00T magazine are US based and uses US English in documentation. The documentation spellings may be edited based on the location for release.
## 2.6 Dates
Date format is set per project. In most cases, the date format is:

%Y-%m-%d-%H:%M:%S
2020-07-09-09:14:22

Do not abbreviate days or months when written.

Correct:	January
Incorrect: 	Jan.

Take global date formats and time zones into consideration when reaching global audiences.
##  2.7 Lists
When creating content for the web, remember to use enumerated lists over entering numbers directly for accessible friendly content.
### Numbered or Ordered
Use numbered lists to indicate order. There is never a list of 1.

Correct:
1. Open the document.
2. Edit the content.
3. Save

Incorrect:
1. Open the document.

###  Bulleted or Unordered
Use bulleted lists to indicate a list without a specific order. There is not a list of 1. Watch for “stray bullets” or areas of content formatted with the bullet formatting but has no text.

  Correct:
• bread
• peanut butter
• fluff

Incorrect:
• bread

## 2.8 Numbers in text
For matters of style, choose between whether the numbers in text will be written out or in numeral format. Whichever makes sense for the project is the best fit. Remember, creating a style guide and setting style guidance prior to creating content will potentially save time during the content creation process.

Avoid starting sentences with a numeral.
Use textual content over numerals for numbers under 10.
Use common sense when using numbers.

Five of six developers prefer bash.
Over 3,000 Debian developers were surveyed about the use of twm over gnome.
## 2.9 Front Matter
### Title page
A title page is the introduction to a guide or book. A title page typically includes:

• Title
• Author
• Publisher/brand
### Copyright Notice
This will typically include a notice of copyright, the copyright owner and ISBN, if published.
The copyright notice will usually include the country the book is manufactured with specific terms or Limitations of Liability.

GNU Linux Manual of Style
Copyright (c) 2020 GNU Essentials, FAQ  Linux, A-Z Tech Writing, Queen Creek, AZ
ISBN:  Number here

Manufactured in the United States of America

You may distribute it and/or modify it under the terms of either the GNU General Public
License (http://www.gnu.org/licenses/gpl.html), version 3 or later, or the Creative Commons
Attribution License (http://creativecommons.org/licenses/by/4.0/), version 4.0 or later.

Trademarks: Linux® is the registered trademark of Linus Torvalds in the U.S. and other countries.
### Dedication
The dedication font style will vary based on the project or organization.
### About the Author
Information about the author is provided here. Bold the first instance of the author’s name.  
### About the Editor
Information about the editor is provided here. Bold the name of the editor’s name.
### Acknowledgements
If there are any acknowledgments, provide these here.
### Table of contents
Your preference. Usually generated using Styles.
### Preface or Abstract
Your preference
### Other
Other front matter items include dedication, notices, disclaimers, etc.
## 2.10 Back Matter
Back matter includes items found in the back of the book such as Appendix, Index, Glossary, and Bibliography. Decide for the project style which format of Appendix to use. For FAQ Linux, use capital letters such as:

Appendix A.  	man pages
Appendix B. 	logs
Appendix C.	Common Words
Appendix D.  Common Trademarks
2.11 Cross References
Use cross-references to link to other material within the documentation. Create a support and maintenance plan or workflow to maintain cross-references.
2.10 Table of Contents and Index
A table of contents assists the reader with navigation. An index is also useful for finding information in documentation. If the index is easily generated and maintained, this is a useful addition to any guide.
## 2.11 Measurements
Set standards for measurements with consistency. Examples are found in the Measurement Abbreviations.

Table 2-1. Measurements Abbreviations
Measurement with Abbreviation

kilobyte
KB

kilohertz
kHz

gigabit
Gb

gigabyte
GB

gigahertz
GHz

megabits per second
Mbps

megabyte
MB

megabytes per second
MB/s

megahertz
MHz

petabyte
PB

terabyte
TB

## Versions
Select version control and version numbering. Kernel and code version standards are set by the projects. An example is the Debian project version guidelines. Prior to release, the version is 0.x. After release, the version is 1.x and above. Documentation version control using similar numbering is recommended.
