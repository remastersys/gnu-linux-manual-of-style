# Chapter 4. High priority - common issues

In documentation, sometimes bad characters are inserted into the process. The characters are non-functional. See information about converting processes.

Additionally, with the need for more documentation in corporate and  other projects, command prompts were riddled with errors by authors and writers not validating against documentation.

While validation does require time and resources, one pass is recommended.
## 4.0 Acronyms
First time use of an acronym is spelled out and where applicable is included in a table. An example is provided.
### Example:	First use of Acronym

Correct: 	Many organizations are considering privacy matters for Artificial Intelligence (AI) and recognition as a service.
Incorrect:	Many organizations are considering privacy matters for AI and recognition as a service.
### Example Table: Acronyms and Abbreviations

Acronyms  and Abbreviations
Description
AI
Artificial Intelligence
API
Application Programming Interface
CLI
Command line interface
DMCA
Digital Millennium Copyright Act
GNU
GNU’s not Unix
IoT
Internet of Things
ML
Machine Learning
OS
Operating System
## 4.1 Internet of Things or IoT
### sda or sdX
When authoring instructions, never explicitly declare a drive, especially with a command like dd. This is a courtesy to users who may simply copy and paste commands in instructions.

Correct:
Copy the SD to your local machine. Using dd and the status=progress flag, you can view the copying status while copying.

Use sudo or switch to superuser to run these commands.

As root:

dd if=/dev/sdX of=imagename.img status=progress

After the image is copied locally, force a sync of any I/O.
As a precaution, use sync.

sync

Incorrect:

### Never explicitly declare the drive.
## Edge Computing
When writing about a topic like Edge computing, a brief overview of Edge can establish credibility. Multiple articles exist about Edge computing and cloud, but never describes the difference.
### Edge Third Party
Avoid using any Linux Foundation projects, tools or platforms. In fact, avoid 3rd party websites and platforms where possible. When the third party website breaks, so will your docs and perhaps reliability in your project may suffer. In documentation, ensure proper use of Trademark and branding for organizations like Linux Foundation using trademarks.
## 4.2 Cultural issues
Cultural knowledge or cultural sensitivity is important in documentation.
### Proprietary v. Not Proprietary
Do not bundle a GNU Linux Operating System with a Proprietary System
### Two or more Distros
Documentation including 2 or more systems can be confusing, and a developer experience challenge.
Make sure to separate the two systems clearly.
### Order of Operating Systems
Listing operating systems in alphabetic order is recommended. Keep documentation consistent with listing operating systems.

• GNU Linux
• Windows
### Adding the full Operating System name
There are some cultural sensitivity when leaving GNU out of GNU Linux. List GNU where GNU is used.
### Electron apps
Avoid recommendations or instructions using 3rd party or non-free applications/tools and utilities.
### Avoid recommending software
Do not write advertisements or base articles on advertisers. Avoid bias.
Give the user choices and options where applicable, while maintaining clarity. However, when a tool, utility or product performs best for the job at hand, list this tool.
### Default installed in the distro v. Install a 3rd party app
When a distro has a tool or utility to perform a task, there must be a very good reason to install an alternate tool. For example, guidance to install etcher is an extra step to simply using an image burning tool already installed by default in a distro.
### No Shills
Do not accept gifts, money, conference bribes, or any quid pro quo situation for writing “fashionable” or “favorable” articles. When advertising a product for a company or project, be transparent.
### Inclusion
As a free and open community, we are already all inclusive. Use non-bias language in documentation.
Do not use pronouns or second person. For R00T stories, use first person. Avoid third person pronouns regarding gender. Gender-neutral.

Suggestion
Replaces
Chairperson, chair
chairman
People, humans
mankind
Built-in
native
primary/secondary
main/secondary
host/device
master/slave
Deny/Allow (describe the list)
Denylist/Allowlist (not preferred)
Refused/Permitted
blacklist/whitelist

## 4.3 Text Editors
Remember, the use of IDEs and text editors are user’s choice. Do not include instructions in vi when the user base may include emacs users. There are multiple editors available and many different “camps”. The following editors may be used by a variety of users.

• emacs
• vi
• nano
• jupyter notebook
• atom
• gedit

Be aware of the color configurations when taking screen captures.

Example: Do not use red callouts with a purple text displaying in gedit.

Use the color wheel to find complimentary colors.
• Yellow and purple
• Blue and orange
• Red and green
## 4.4 Spacing
Do not waste real estate. Too much leading or spacing above or below a heading can be distracting. Linear websites with a lot of space require scrolling and navigation. Make spacing even and sensible.
## 4.5 Clear Communication
Avoid the following words in content.

• that
• needs to be
• ought to
• should
• better

1. Do not use "it"
2. Avoid "which" and “that”
3. Clean up language - "the tutorial" "on"
4. Use variables like $HOME or $INSTALLDIR where it makes sense over a path
5. Break down long commands for users. A table works well.

This command writes the image to the target device.

sudo dd if=imagename.img of=/dev/sdX status=progress

Command
Use
Description
sudo
sudo
Require root
dd
dd
dd command – convert and copy a file.
if
if=imagename.img
Defines the origin. In this case, the image file to be written.
of
of=/dev/sdX
Defines the target, in this case, the drive. An X replaces a drive letter. Use df -h or fdisk -l to determine your drive.
status
status=progress
Displays the progress until finished.

## 4.6 Multiple Operating Systems
When writing about different operating systems, give equal time to both. If you do not have equal documentation for both operating system types, this is a gap. Separating instructions is recommended.

Instructions for the following systems:

• Debian
• Windows
• MacOS

One exception to separation may be if the documentation compares the operating system instructions. Simple instructions may also be clear.

However, lengthy documentation for each section is not recommended. In this case, separate the sections. If the user navigates to the wrong section, this can waste time. Linear pages can be frustrating when attempting to install, configure or run instructions.

When including information about installation, tutorials and where to find the developer guide, match the content for all operating systems when you can. Use some ordering as from a user experience perspective, offering instructions for several operating systems can be a challenge. In some cases, different distros can be a challenge. However, documentation for different distros can be very clear by labeling the instruction set by distro. Give each distro a heading or some emphasis.
### MorphOS
Instructions
Amiga
Pegasos
### RedHat
Instructions
x64
ARM

Lengthy instructions for separate operating systems can be unclear. Clear instructions are important for the audience to quickly go to the section or instructions.
## 4.7 Website Images
### Size
Add images of a consistent size. This can be configured in Drupal or WordPress if you are using either of these systems.
### Zoom
Zoom features are recommended. A zoom function/magnifying glass would be nice but not sure how your system or CMS manages this.

An option may be to link to the full version of the image. For example, if the user clicks on the image, this trigger will open a page with a larger version of the image.
### Clear
No blurry images.
### Crop
When cropping images, maintain a consistent crop area.  Example, include the scroll bar in all or crop the scroll bar from all.
### Favicon
Check your faviocon on different browsers and take precautions with security issues
## 4.8 Infographics
Creating infographics for projects can be as simple as using a common template in GIMP. Alternate methods include using a third party platform with design templates.

Infographics are a great way to offer visual content to users. Audiences include visual learners and infographics may offer a clean way to display content.
### Print
150-300 ppi is recommended, depending on the size of the printed infographic.
### Web
Remember web content requires 72 ppi.
### 4.9 Key Commands
Ctrl+Shift+Alt+G
Ctrl+fn+F4

Note: In writing fn key is great. When you speak this out loud – the “fn key” can be misunderstood by the person receiving audio communication.

## 4.10 FAQs or Troubleshooting Tips
Where there may be a need for a Frequently Asked Questions or Troubleshooting tips, include this in the documentation. Often this will be an appendix or at the end of a guide. There may be tips throughout instructional documentation. For web content, an accordion or drop down list may be available.
### Reading and Understanding logs and errors
Understanding logs and errors is a great  way to create a troubleshooting section, resolve errors and improve documentation. Qualified free and open source software content creators will assist in documenting Frequently Asked Questions, bug reports and errors. There is a difference between a configuration and a bug. Be transparent.
