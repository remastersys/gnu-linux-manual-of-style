## Copyright (c) GNU Essentials
ISBN:  978-1-71683-468-4

Manufactured in the United States of America

You may distribute the GNU Linux Manual of Style and/or modify the GNU Linux Manual of Style under the following terms:

## Code
GNU General Public
License (http://www.gnu.org/licenses/gpl.html), version 3 or later

## Content
Creative Commons
Attribution License (http://creativecommons.org/licenses/by/4.0/), version 4.0 or later

## Trademarks
Respecting the choice to trademark by others, we do not have trademark rules for GNU Essentials, FAQ Linux, R00T magazine or A-Z Technical Writing.

Linux® is the registered trademark of Linus Torvalds in the U.S. and other countries.
