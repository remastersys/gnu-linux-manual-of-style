# CHAPTER 1. The Documentation Development Life Cycle

1.0 Basics of Technical Authoring
 **Tell them!**
* Intro: Tell them what you are going to do
* Body: Tell them how to do it
* Summary: Tell them what you did
* Next Steps:Tell them what they can do next

An outline can be as simple as a list prepared in your mind’s. When I read this in a college textbook, I doubted this but over time, you may discover your organizing the outline in your mind prior to ever putting the content down on a page.
# 1.1 Documentation Development Life Cycle
The following are components of the documentation development life cycle.
• Requirement Analysis
• Audience Analysis
• Document Outline
• Prototype
• Develop
• Review
• Publish
• Maintain/Support

## Requirement Analysis
Similar to a needs analysis but deeper into the requirements of the documentation after the need is established. What documentation is required? Just a user guide? Additional Administration guide? Application note?

A sample template is provided.
Requirement/Needs Analysis Template
Question
Response
Do we need documentation?
[  ] Yes    [  ] No
Why do we need documentation


List reasons why below
• man page vague
• some users do not read man pages
• options not self explanatory
• support community
Proposed time frame

Proposed team members and contribution percentage or hours
Technical Writer/Editor 20 hours a week
Developer 5 hours a week
Project Manager 1 hour a week (basic task management - could be managed by technical writer or developer alternately)
Testing - Validation and review - depends on project size and scope
Proposed documentation required
[  ] Installation Guide
[  ] Get Started Guide
[  ] User Guide
[  ] Release Notes
[  ] System Administrator Guide
[  ] Tutorials
[  ] Video demos
[  ] Reference Guide
[  ] Quick Start Guide
[  ] Reference Card
[  ] Application Notes
[  ] Product Brief
[  ] Product Specification
[  ] Whitepaper

[  ] Facilitation Guide
[  ] Student Guide

Style Guide exists?
[  ] Yes   [  ] No  
If Yes, Where?
Question
Response
Other considerations
• accessibility
• privacy
• security
Proposed Video/audio
[  ]  podcast
[  ]  streaming live video interactive webinar
[  ]  live demo
[  ]  recorded demo
[  ]  twitch stream
[  ]  audiobook
Content Management
Where do existing videos reside?
Where will future content reside?
What version control?


## Audience Analysis
Analysis of primary and secondary audiences – template example is included. This is a starter template. Insert additional information in the response field.
Audience Analysis Template
Project: Living on the Edge - AI at Home
### Primary Audience Profile
• Maker
• Entrepreneurs
• IOT Developers
• Enterprise Developers

Question
Response
What is the job function and role of the primary audience?
Best Guess
How will this audience use this document?
For reference with hands-on samples/demos
What is the educational level of this audience?
[  ] new [  ] can hack it [  ] some knowledge  
[  ] expert
How experienced are the members of this audience with IoT?
[  ] new [  ] can hack it [  ] some experience
[  ] expert
How experienced are the members of this audience with AI or machine Learning?
[  ] Beginner [  ] Intermediate [  ] Advanced
What is their work environment like?
[  ] Lab [  ] Desk/Office [  ] Remote
What is their interest level?
[  ] Low [  ] Medium [  ] High
What biases, preferences, or expectations, might they have?
[  ] Low [  ] Medium [  ] High
Which operating systems are they familiar?
List here:   
How much theory or “nice-to-know” information will they want?
Insert info here:
What other training will they receive in addition to the documentation?
[  ] samples [  ] tutorials [  ] training [  ] webinars [  ] other:
### Secondary Audience Profile
Question
Response
What is the job function and role of the secondary audience?
Best Guess
How will this audience use this document?
For reference with hands-on samples/demos
What is the educational level of this audience?
[  ] new [  ] can hack it [  ] some knowledge  
[  ] expert
How experienced are the members of this audience with IoT?
[  ] new [  ] can hack it [  ] some experience
[  ] expert
Users experience vary. Level varies from beginner to expert. These are school instructors.
How experienced are the members of this audience with AI or machine Learning?
[  ] Beginner [  ] Intermediate [  ] Advanced
What is their work environment like?
[  ] Lab [  ] Desk/Office [  ] Remote
Educational office environment for administrative staff or teaching environment for instructors, where they must have a basic computer for scheduling their courses with resources and standards.
What is their interest level?
[  ] Low [  ] Medium [  ] High
What biases, preferences, or expectations, might they have?
[  ] Low [  ] Medium [  ] High
Which operating systems are they familiar?
List here:   
How much theory or “nice-to-know” information will they want?
Insert info here:
What other training will they receive in addition to the documentation?
[  ] samples [  ] tutorials [  ] training [  ] webinars [  ] other:

### Prototypical User example:
The prototypical user will be a teacher who is preparing the coursework for the class. This may need approval or align with the teaching requirements.
## Document outline
Draft the initial outline based on the requirements and need. If one is not already created in the needs analysis stage, create documentation objectives. These objectives are similar to learning objectives and include:

• Purpose
• What will be covered or included
• Goals

Example Objectives
Purpose: To document the new module for the web application, iGNULearn
What will be included: Each option, configuration, system administration and user navigation
Goals: To document the new module completely for users and system administrators

The outline may differ depending on type of documentation needed. An article for developers  for example, may use a standard content strategy familiar to other developers. Drupal or Wordpress modules have a standard content strategy for the most part. The description and version are included as well as other information.

For repeated content needs, a strategy can be followed, not for the sake of consistency alone, but for the sake of including those important key elements necessary to reach your audience.

To create an outline, consider the organization and flow of the document. In the case of software documentation, this can be done by section. Each section can be outlined per the requirements and features. A troubleshooting section is often found at the end of the guide.
### Brainstorm topics
There are many brainstorm techniques. Use the method best for you and project needs.

• notes
• lists
• outside perspectives


Organize topics. The type of organization or order will vary. Select an order during kick off or as soon as possible prior to writing the draft.

• chronological
• functional
• by component
• alaphabetic
• features
• other
### Starter Outline
Chapter 1. Overview
Chapter 2. Introduction to Machine Learning
Chapter 3. Pre-trained models
Chapter 4. Optimization
Chapter 5. Application 1 – Object detection
Chapter 6. Application 2 – Emotion recognition
Chapter 7. Other use cases
Chapter 8. Conclusion
Chapter 9. Future work and resources
## Prototype
This is a content management or strategy step to prepare in advance for content creation. Will include:

Style guide creation or updates
Template creation or updates
Content Strategy standard creation or updates
Workflow documentation or process improvements
Change Management procedures

Note:	When working with older or proprietary systems, remember to write GNU Linux documentation on the same system as is being documented. An example would be to write documentation requiring Debian Stretch with kernel 4.15 on Debian Stretch with kernel 4.15. If a dedicated system is not a possibility, consider using a virtual machine.

Convert proprietary processes to non-proprietary where the conversion improves efficiency.
### Style Guide
Style guides assist the authors to create consistent looking documentation readers will come to understand and expect. Visual cues for readers enable your audience to quickly get to the information they are looking for. One example is a caption. An example of a caption style is:

Figure 1. This picture is about this topic

Captions must be notable and descriptive.
This way, someone searching for a particular image or topic, there is more information about what the configuration would look like, if the caption is descriptive. Consider the time of the reader is as valuable as your own. If you keep in mind the audience and respect the audience, documentation quality is fairly automatic.
### Templates
Create templates to save time and reduce error. Templates are a good way to organize documentation. Templates can be easily customized documentation tools to ensure consistency with the style guide and branding of the project or organization. There is no reason to reinvent the wheel. With styles applied to the document, the compliance with the style guidelines become simple to apply. This is how several documentation projects update and create content for their user guides.

Often a project will use markdown rather than a document editor. Many different tools are available for creating documentation. This will come in a later chapter about tools in the content creator toolbox.

Templates can include the yaml files needed to generate html or the file hierarchy for generating SCORM compliant learning and documentation PDFs using single sourcing, XML, XSL and DITA. You can even have a template for rolling out simple debs based on a script.

The project needs will vary. However, if the templates are shared and accessible, a library of templates can be a valuable resource for any project!
19. Content strategy: Web based article or guide
One winning formula for content strategy is found in Drupal and Wordpress modules. For Drupal, the modules include information about what the module is and version control. Important also is how it works. Often, digging deeper, instructions or demos were available. This information is a quick path to installing a module or plugin and could work for your quick start or get started guides as an overview.

Add a tutorial after the demo to enable developers to configure or custom the original demo to their needs.

Of course, your project can use different phrases to fit your brand.
• Description
• Features
• Screenshots
• What’s new video
## Example Strategy
Overview or Introduction
Short overview - “tell them what you’ll tell them”
What it is
Details about what it is

How it works
Details about how it works
Architectural diagram where useful

Main Content “tell them”
Prerequisites to perform demo
Hardware
Software

Estimate of time to perform demo
Just a short estimate can really assist a developer or reader to understand how much time to schedule or dedicate to this demo. This really does make a significant difference to some busy devs. Try to be as accurate as you can because otherwise, what’s the point?

Installation
An example may be an installation section in a get started guide. Installation may be as simple as apt install <tool>. Therefore, there really is not a lot of need for installation guidance, depending on your project. Keep installation sections simple.

Demo - Instructions for hello world
People enjoy the blinking led or simple action
This reinforces the intent and can inspires or motivate
Success is essential

Demo with a twist
Lead the reader based on previous demo to execute instructions similar to enable developers to run more future applications

Conclusion
“Tell them what you told them”
References - resources - learn more
Include links or references to a few more relevant resources.
Always remember, when linking to third party or outside resources, you run the risk of 404!
## Outline: Web based article or guide
Overview
What it is
How it works
Main Content
Prerequisites to perform demo
Hardware
Software
Estimate of time to perform demo
Demo - Instructions for hello world
Demo with a twist
Conclusion
References - resources - learn more
## Workflow
Understanding workflow improves quality and time. When the workflow is documented, locating the correct logs and information to troubleshoot is clear.
## Project Workflow
Documenting workflow can make a huge difference in completion of a project. Once the workflow is determined, the project components are more clear. An example is a GNU Linux server not connecting to another server. A file is not transferred and the process is incomplete. This happened on a project once. The QA department did not even realize the origin of the file being transferred was from a legacy GNU Linux server.
## Documentation Workflow
• How will the author or technical communicator create documentation?
• What tools will the author/publisher use?
• Does a prior standard or workflow exist?
• Can this be converted to a non-proprietary process?
• What is the collaboration process?
• What are other projects doing with success?
• Can the workflow be improved or updated?
## Converting workflows to non-proprietary
Some of the most fulfilling tasks can be to automate work to complete a job more efficiently. Writing scripts and using non-proprietary software workflows can enable the content creator to save time and resources while maintaining error free content.
## Example 1. Non-Functional non POSIX characters
Use a script or grep/sed commands to find and replace odd non POSIX characters in content. At times, you may find non POSIX characters in copy code snippets or commands.

Correct character for commands: 		-
Incorrect character for commands:		–

While the difference may not be obvious to the common observer, the technical writer or developer will note this character will NOT work at the command line. This will cause the command to fail. This slows down your reader. When this happens multiple times, this can be frustrating and call into question the reliability of the documentation.

These characters are inserted in the documentation in several ways:
• Windows users and MS Word
• Windows users and MS Outlook
• GNU Linux users and LibreOffice settings

There are ways to configure the settings for these applications to avoid the conversion from a dash to a non POSIX character. Using an IDE or GNU Linux application can prevent this type of error. If you receive documentation with the error, a simple script can be run against the documentation to replace the incorrect character with the correct character.

Note: If you are using the non-functional characters for some reason other than in code or commands, note this when implementing your replace method. All of this being said, when GNU Linux writers create content using GNU Linux, these types of errors are typically not a problem.

```## Ugmug

#!/bin/bash

# Author:  marcia wilbur GPLv3
# Purpose: validate and repair functionality for Linux if
# 	     using MS writers
# Usage:   grep character path/to/*.md

# ms dash: not functional

echo "replacing MS weirdness"
result=$(grep – filename.md)
echo $result
sed -i 's/–/-/g' ./*.md
echo "below are the instances of non functional dash (–)"
cat filename.md | grep '–'
Code sample 1. dash
Often in documentation the wrong non-functional quotes will be present. Replace quotes using a script or command such as the script below.

# literal quotes

echo "replacing literal quotes"
$quotes=$(grep "` filename.md")
echo $quotes
sed -r 's/\\\`/\\'/g' ./*.md
echo "below are the instances of the literal quote"
cat filename.md | grep `
```

## Change Management
One great benefit to keeping a good changelog is when someone receives updated documentation, the changelog can be used to minimize the research for what was changed. In documentation, often a revision history is placed in the front matter. An example is provided.

Date
Revision
Description
06-06-2020
0.1
Initial Draft
06-09-2020
0.2
Added Change Management
06-28-2020
0.3
Add Revision History
 Develop Draft
# Create documentation
## Document process and workflow for pass off
Different projects will have different ways of organizing, collaborating and collecting data. Here is where you can turn the data into information and present the content to the audience in the way set out by your style, outline and brand.

Use the templates to create  your documentation. With the outline, style guidance, templates and workflow, you have all the tools needed to create documentation.

How you gather good content and develop the draft is dependent on your project.

Often a project will have 0 documentation. Where can you start developing the draft?

• Collect data
• Gather and verify content and organize
• Interview developers
• Read Code Requirements
• Read Code
• Alpha/Beta test the software – sandbox available?
## Fill the GAP
Do not assume the reader knows information!

When writing documentation, remember, the reader may not have the same background, experience and knowledge as you. Often technical communicators are employed to fill the gap. What may seem obvious to an engineer or developer may not be obvious to a new developer or new grad.

Example: Explanatory instructions with value
Incorrect: 	Use -r with uuidgen
Correct: 	The -r flag is used to generate UUIDs version 4 - random numbers.
## Quality Assurance Testing v. Usability Testing
The difference between Quality Assurance Testing and Usability Testing is one is based on success of the features and the other is based on success of human computer interaction. Historically, technical writers are the experts in usability. In fact, Arizona State University recently added a concentration in Usability for their technical communications degree.

• Validate software (QA)
• Conduct usability tests (DX/UX)

Participant testing either prior to publishing or after publishing can offer excellent insight about how your readers use the documentation and product. However, a participant pool of 4 people doesn’t really tell a great story or give great data.

The quality and quantity do matter in testing. While considering participants for the testing, criteria must be set forth. When the participant testing criteria is set forth by the developers, useful testing may move forward. Without correct participant pool, testing and criteria, testing is of little to no value.
## Review
There are several types of reviews performed for documentation. The tasks are different for each type of review.

As a technical editor, my tasks included running code snippets to ensure or validate the code.
As a technical proofreader, tasks were different and included grammar and technical language.
There are several main reviews to perform for documentation prior to publishing. Depending on your project needs, you may perform none of these or more than one of these. However, keep in mind, review only works when valid feedback is presented and improvements are made based on feedback.

1. Valid Feedback
2. Responsive to feedback
3. Implement quality improvements based on feedback

Types of review prior to publication:

Functional	Review (usability, useful)
Editorial	Review (grammar, copy edit)
Legal		Review (copyright, trademark)
Peer		Review (unit/project standards)
Technical	Review (usability and correct)

Do you have a review policy, process or software?

Careful when reviewing documentation to select reviewers qualified to review the documentation without any conflict of interest.
Example: Do not give a report the task of reviewing a manager or supervisor’s documentation.
### Functional review
This review is also considered validation. Use internal resources to conduct validation/testing
Some examples include: interns, other developers, people not familiar with the product but with the developer background, anyone able to perform the validation. When a technical writer understands the content or has the ability to perform the functional review, this is ideal. However, since the writer did create the content or worked on the content, another editor would validate against the documentation and provide feedback.
You could create templates for validation. However, casual validation with the participant/tester providing feedback notes is often enough data.
### Editorial review
This is the review done by another technical writer. The focus of this review is grammar, tone and language.
### Legal review
This is the review done by another technical writer. This can include the legal department. Often, legal will review documentation on their own time without meetings with the technical writer. The focus of this review is legal matters such as trademark, copyright, partnerships, potential liability, licenses, and such. You may not require or have legal representation for the project, but a trademark, license and copyright review is important for some documentation.
### Peer review
The peer review is conducted by another technical writer familiar with the project. This review can provide feedback to various ways the documentation can be improved. However, this requires more than one technical writer for the project.
### Technical review
During this review, the technical writer will review the technology, code and architecture to ensure these are working and make sense.
### Participant testing
This is often conducted after documentation is drafted. Official developer testing at an outside lab gives feedback, not just about documentation, but how to improve the developer experience. There is a chapter about the GNU Linux Developer Experience later in this book.
# Publish
After the general agreement or upon the timeline deadline for publishing, publish the documentation.
Your project or organization’s process may differ from others.
## Media Modes
• Web content in CMS or website
• printed materials
• ebook
• PDF
• markdown in git or on git server
# Maintain, Update and Support
There is a significant need to keep documentation updated  to maintain usability. In some cases there are security issues and patches or even such as the case with tensorflow* deprecation of types. Stale information must be updated. Stale information or old non-functional docs are not useful or usable.

Depending on your project, support for the product or documentation support will consist of maintaining the documentation and updating information. Other lines of communication to support users visiting your documentation can include:
• Slack
• Forum
• IRC
• Email
• Ticket system
• Chat
• Phone
# 1.2 Example: Get Started Guide
To get started, the main sections of a get started guide might include:
1. Documentation or Contents - Level 1
2. Run an application - Hello World
3. Modifications/configurations
4. Projects and Tutorials
5. Next Steps

A template for guides is found below.
## Introduction (required)
Introduce the project/product/topic of interest. List what will be discussed.
##  What is it?
Description
##  Objectives (optional)
• Never enumerate
• Use bullets
• Use action verbs
Example:  Objectives
Upon successful completion of these instructions, you should be able to:
• Understand how time/date stamps work
• Configure the script to include a time/date stamps in python
• Run inference with time/date stamp output
How It Works (optional)
This section describes:
• Design summary
• Architectural Diagram
• Components
• Services
## Get Started (required)
Describe the basic steps for the guide.
## Before you begin (required)
• Describe the prerequisites
• List requirements outside of the device hardware
## Hardware requirements (required)
Add information for device hardware.
• Processor Information
• RAM
• Storage minimum
• Internet connection
## Software requirements (required)
Add information for software.
Operating System
Dependencies
Other packages
## Skills (optional)
Familiar with GNU Linux commands

Note: Do not list default software included in the particular OS, spin or distro. Do not guide users to install 3rd party software when applications exist in the distro already.
## Example 1: Required software
No need to list bash for Debian
Do not lead users to install 3rd party software unless necessary
## Example 2: Default software
Do not lead users to install Etcher on Ubuntu when startup disk creator will do the job just fine and installed by default.
## Install (optional)
## Steps (required)
Include the steps needed to complete the task.
• Install prerequisites (required)
• Run (required)
• Stop (optional)
• Clean (optional)
## It Works! [hello world]
Describe the sample or introductory exercise
Include an architectural diagram
## Modify Steps (optional)
Include the steps needed to try, configure or customize.
• Install prerequisites (required)
• Run (required)
• Stop (optional)
• Clean (optional)
## Tutorial(s) (optional)
Describe the tutorial(s).
## Tutorial 1: Title of Tutorial
• Describe the purpose of the tutorial.
• Add an Architectural Diagram.
• Include Objectives (upon successful completion)
## Steps (required)
Include the steps needed to complete the tutorial.

## Step 1: Name of Step
• Describe the step.
• Include step instructions

## Step 2: Name of Step
• Describe the step.
• Include step instructions

## Step 3: Name of Step (optional)
• Describe the step.
• Include step instructions
## Additional tutorials (optional)
## Summary (required)
Discuss tutorial(s)
## Next Steps (required)
Describe where to go or what to do next
Example: Relevant demos, similar projects
## References (optional)
URLs to other documentation.
## Code Samples (optional)
URLs to code samples or repos.
## Additional Information (optional)
URLs for additional information.
• Links to other resources
• Links to additional resources.
## Troubleshooting (optional)
Include any troubleshooting tips.
## Legal (optional)
Include Legal information.
